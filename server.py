#!/usr/bin/env python3

import socketserver
import threading
import time

from lib.logger import TerminalLogger
from lib.http_thread_server import HttpThreadServer
from server_communication import ConfigSyncHandler
from web.handler import WebHandler

DEBUG = True  # FIXME: CLI or environ?
NAME = 'Easy Parental Control'


class ConfigSyncServer(socketserver.TCPServer):
    daemon_threads = True       # hopefully will cleanly kill all spawned threads
    allow_reuse_address = True  # claimed faster rebinding

    def __init__(self, ip: str = ConfigSyncHandler.server_ip, port: int = ConfigSyncHandler.server_port, web_port: int = 8080, handler_class=ConfigSyncHandler):
        self.logger = TerminalLogger(file_path=__file__.replace('py', 'log'))
        self.logger.debug(f'server init: {ip}:{port}')
        self.http_server = HttpThreadServer((ip, web_port), WebHandler)
        socketserver.TCPServer.__init__(self, (ip, port), handler_class)  # daemonised (second instance)


def main():
    s = ConfigSyncServer()
    t = threading.Thread(target=s.serve_forever)
    t.daemon = True
    t.start()
    s.http_server.start()

    while True:  # FIXME: wait for out-side termination
        time.sleep(60)


if __name__ == "__main__":
    main()
