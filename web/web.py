from server_communication import ConfigSyncHandler


class Web:
    NAME = 'Parental Control'
    META = '<meta http-equiv="refresh" content="60">'
    CSS = '''<style></style>'''
    JS = '''<script>
      function mousedown(id) {{ var xhr = new XMLHttpRequest(); xhr.open("POST", "", true); xhr.send(id+"=PRESSED"); }}
      function mouseup(id)   {{ var xhr = new XMLHttpRequest(); xhr.open("POST", "", true); xhr.send(id+"=RELEASED"); }}
    </script>'''

    @staticmethod
    def home_page(path) -> str:
        short_config = ''
        for i, c in enumerate(ConfigSyncHandler.configs.configs):
            short_config += f'''
                <form action="/modify_time_left" method="post"><font size="5">{c.user if c.user else c.client_ip}:\t{c.time_left_min}</font>
                    <input name="{i}p10" value="+10" type="submit" style="font-size:50px"/>
                    <input name="{i}m10" value="-10" type="submit" style="font-size:50px"/>
                    <input name="{i}BAN" value="BAN" type="submit" style="font-size:50px"/>
                </form>'''
        return f'<html><head>{Web.META}<title>{Web.NAME}</title></head><body>{short_config}</body><html>'

    @staticmethod
    def debug(path) -> str:
        ful_configs = ''
        for c in ConfigSyncHandler.configs.configs:
            ful_configs += '<br>'.join(str(c).split(', ')) + '<br><br><br>'
        return f'<html><head>{Web.META}<title>{Web.NAME}</title></head><body>{ful_configs}</body><html>'

    @staticmethod
    def page(path: str) -> str:
        sub_page = path.rsplit('/', maxsplit=1)[-1]
        return {
            '': Web.home_page(path),
            'debug': Web.debug(path),
            'modify_time_left': Web.home_page(path),
        }[sub_page]

    @staticmethod
    def image(path) -> any:
        image_name = path.rsplit('/', maxsplit=1)[-1]
        raise NotImplemented(f'Unknown image {image_name}')
