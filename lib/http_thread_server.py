from http.server import HTTPServer, BaseHTTPRequestHandler
from typing import Type

from lib.utils import who
from lib.threading2 import LoggingExceptionsThread


class HttpThreadServer(LoggingExceptionsThread):
    """ HTTP server ever serving in its own thread. Any exception aborting it get logged and retry is performed. Every retry happens with higher retry delay to limit performance impact. """

    def __init__(self, address: tuple, handler: Type[BaseHTTPRequestHandler]) -> None:
        super().__init__()
        self.address = address
        self.handler = handler
        self._server = None
        self.retry_delay = 0

    def iterate(self):
        self.cleanup()  # in case re-try?
        self.event_stop.wait(self.retry_delay)
        self.retry_delay = 2*self.retry_delay + 1

        self.logger.info(f'{who(self)} starting at: {self.address}')
        self._server = HTTPServer(self.address, self.handler)
        self._server.serve_forever()  # blocking, throwing exceptions > logged, retry by next iteration

    def cleanup(self):
        if self._server:
            self._server.server_close()
            self._server.shutdown()
            self._server = None

    def stop(self):
        super().stop()
        self.cleanup()
